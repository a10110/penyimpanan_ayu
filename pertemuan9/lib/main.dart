import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main()
{
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Pemrograman Perangkat Bergerak',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home:MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState()
  {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime date = DateTime(2022, 03, 22);

  void _showDialog(Widget child) {
    showCupertinoModalPopup<void>(
        context: context,
        builder: (BuildContext context) => Container(
          height: 216,
          padding: const EdgeInsets.only(top: 6.0),
          margin: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          color: CupertinoColors.systemBackground.resolveFrom(context),
          child: SafeArea(
            top: false,
            child: child,
          ),
        ));
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Pemrograman Perangkat Bergerak",
          style: TextStyle(color: Colors.white),),
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        alignment: Alignment.topCenter,
        child: Column(
          children: [
            Text(greetingMessage(),
              style: TextStyle(
                color: Colors.redAccent,
                fontSize: 25,
              ),
            ),
            SizedBox(height: 20,),
            CupertinoButton(
              child: Text("Pilih Program Studi",
              style: TextStyle(color: Colors.white),),
              onPressed: () {
                showActionsheet();
              },
              color: Colors.blueAccent,
            ),
            SizedBox(height: 20,),
            CupertinoButton(
              child: Text("Pilih Tanggal Lahir",
                style: TextStyle(color: Colors.white),),
              onPressed: () => _showDialog(
                CupertinoDatePicker(
                  initialDateTime: date,
                  mode: CupertinoDatePickerMode.date,
                  use24hFormat: true,
                  onDateTimeChanged: (DateTime newDate) {
                    setState(() => date = newDate);
                  },
                )
              ),
              color: Colors.redAccent,
            ),
          ],
        ),
      ),
    );
  }
  showActionsheet()
  {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) =>
          CupertinoActionSheet(
            title: Text('Pilih salah satu'),
            actions: <Widget>[
              CupertinoActionSheetAction(
                child: Text('S1 Informatika', style: TextStyle(fontSize: 15),),
                onPressed: () {
                  Navigator.pop(context, 'S1 Informatika');
                },
              ),
              CupertinoActionSheetAction(
                child: Text('S1 Sistem Informasi', style: TextStyle(fontSize: 15),),
                onPressed: () {
                  Navigator.pop(context, 'S1 Sistem Informasi');
                },
              ),
              CupertinoActionSheetAction(
                child: Text('D3 Akuntansi', style: TextStyle(fontSize: 15),),
                onPressed: () {
                  Navigator.pop(context, 'D3 Akuntansi');
                },
              ),
              CupertinoActionSheetAction(
                child: Text('D3 Desain Komunikasi Visual', style: TextStyle(fontSize: 15),),
                onPressed: () {
                  Navigator.pop(context, 'D3 Desain Komunikasi Visual');
                },
              ),
              CupertinoActionSheetAction(
                child: Text('D3 Sistem Informasi', style: TextStyle(fontSize: 15),),
                onPressed: () {
                  Navigator.pop(context, 'D3 Sistem Informasi');
                },
              ),
            ],
            cancelButton: CupertinoActionSheetAction(
              child: const Text('Cancel', style: TextStyle(fontSize: 18, color: Colors.red)),
              onPressed: () {
                Navigator.pop(context, 'Cancel');
              },
            ),
          ),
      barrierDismissible: true,
    );
  }
}

String greetingMessage() {
  var timeNow = DateTime.now().hour;

  if (timeNow <= 11.59) {
    return 'Selamat Pagi';
  } else if (timeNow > 12 && timeNow <= 16) {
    return 'Selamat Siang';
  } else if (timeNow > 16 && timeNow < 20) {
    return 'Selamat Sore';
  } else {
    return 'Selamat Malam';
  }
}